/* sven
 * config.h
 * copyright 2021 Zachary Smith
 */

// filename constants
const size_t max_filename_length = 32;
const size_t max_num_pages = 5;

// define filenames
const char *index_file="index.md";
const char *about_file="about.md";
const char *blog_file="blog.md";
const char *template_html="template.html";

const char *pages[5] = {
  "index.md",
  "about.md"
};

// define directories
const char *template_dir="templates/";
const char *stylesht_dir="styles/";
const char *images_dir="images/";
const char *blogpost_dir="posts/";
const char *build_dir="build/";

// auto-generate blog page
const bool blog_gen = true;
