/* sven
 * sven.c
 * Copyright 2021 Zachary Smith
 */

// includes {{{
#include <assert.h>
#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "config.h"
// }}}

#define MIN(a,b) (a < b) ? a : b

// initialize functions {{{
bool begins_with(const char *, const char *);
void blogs_gen();
void blogs_convert();
void change_name_to_html(char *);
void pandoc_convert(const char *, const char *);
bool parse_file(const char *);
void remove_from_end(char *, size_t);
// }}}

struct stat st = {0};

// begins_with function {{{
bool begins_with(const char *str, const char *sub) {
  if (strncmp(str,sub,MIN(strlen(str),strlen(sub))) == 0)
    return true;
  return false;
}
// }}}

// blogs_convert function {{{
void blogs_convert() {
  DIR *d;
  struct dirent *dir;
  char *html = (char *) malloc(512);
  char *outdir = (char *) malloc(1024);
  char *draft = (char *) malloc(128);
  char *findir = (char *) malloc(160);
  strcpy(outdir,build_dir);
  strcat(outdir,blogpost_dir);
  if (stat(outdir, &st) == -1)
    mkdir(outdir, 0755);
  d = opendir(blogpost_dir);
  if (d) {
    while ((dir = readdir(d)) != NULL) {
      if(begins_with(dir->d_name,"."))
        continue;
      strcpy(findir, blogpost_dir);
      strcat(findir, dir->d_name);
      printf("Converting %s... ",dir->d_name);
      if(parse_file(findir)) {
        printf("draft, skipping\n");
        memset(findir, 0, strlen(findir));
        continue;
      }
      strcpy(html, findir);
      change_name_to_html(html);
      pandoc_convert(findir, html);
      printf("done\n");
      memset(html, 0, strlen(html));
      memset(findir, 0, strlen(findir));
    }
    closedir(d);
  }
  free(dir);
}
// }}}

// change_name_to_html function {{{
void change_name_to_html(char *str) {
  char *end = str + strlen(str);
  while (end > str && *end != '.') {
    --end;
  }
  if (end > str) {
    *end = '\0';
  }
  strcat(str,".html");
}
// }}}

// pandoc_convert function {{{
void pandoc_convert(const char *ffile, const char *tfile) {
  char *command = (char *) malloc(1024);
  char *cwd = (char *) malloc(1024);
  strcpy(command, "pandoc ");
  strcat(command, ffile);
  strcat(command, " --template=");
  if (getcwd(cwd, 1024) != NULL) {
    strcat(command,cwd);
    strcat(command,"/");
  }
  strcat(command, template_dir);
  strcat(command, template_html);
  strcat(command, " -o ");
  strcat(command, build_dir);
  strcat(command, tfile);
  system(command);
  free(command);
}
// }}}

// parse_file function {{{
bool parse_file(const char *fname) {
  FILE *fp;
  char *line;
  char *draft = (char *) malloc(256);
  bool drft = true;
  unsigned short counter = 0;
  size_t len = 0;
  ssize_t read;
  fp = fopen(fname, "r");
  if (fp == NULL)
    printf("\nFile %s cannot be opened\n",fname);
  while ((read = getline(&line, &len, fp)) != -1) {
    if (begins_with(line,"draft:")) {
      strcpy(draft,line);
      draft[strcspn(draft, "\n")] = 0;
      remove_from_end(draft, 7);
      if (strncmp(draft, "false", 5) == 0) {
        drft = false;
        break;
      } else
        break;
    } else if (begins_with(line,"---")) {
      if (++counter == 2)
        break;
    }
  }
  fclose(fp);
  free(line);
  return drft;
}
// }}}

// remove_from_end function {{{
void remove_from_end(char *str, size_t n) {
  assert(n != 0 && str != 0);
  size_t len = strlen(str);
  if (n >= len)
    return;
  memmove(str, str+n, len - n + 1);
}
// }}}

// main function {{{
int main() {
  char *text = (char *) malloc(512);
  char *draft = (char *) malloc(512);
  char *file_md = (char *) malloc(max_filename_length);
  unsigned short index = 0;
  char *file_html = (char *) malloc(max_filename_length + 2);
  char *blog_html = (char *) malloc(strlen(blog_file) + 5);
  do {
    printf("Parsing html filenames\n");
    if (parse_file(pages[index])) {
      continue;
    }
    printf("Converting %s... ", pages[index]);
    strncpy(file_md, pages[index], MIN(strlen(pages[index]),max_filename_length));
    strcpy(file_html,file_md);
    change_name_to_html(file_html);
    pandoc_convert(file_md, file_html);
    printf("done\n");
    memset (file_md, 0, max_filename_length);
    memset (file_html, 0, max_filename_length + 2);
  } while (pages[++index] != NULL);
  printf("Checking %s dir...\n", blogpost_dir);
  blogs_convert();
  free(text);
  free(draft);
  free(blog_html);
  return 0;
}
// }}}

