SVEN := sven.c
OBJ := sven.o
HEAD := config.h

all: ${OBJ}
	gcc -o sven ${OBJ}

sven.o: ${SVEN} ${HEAD}
	gcc -c ${SVEN}
